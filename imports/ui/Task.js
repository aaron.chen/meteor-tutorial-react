import React from "react";

export default Task = ({ task = {} }) => {
  const { text } = task;

  return <li>{text}</li>;
};
