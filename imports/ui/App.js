import React, { useState } from "react";
import { useTracker } from "meteor/react-meteor-data";

import { Tasks } from "../api/tasks";

import Task from "./Task";

export default App = () => {
  const tasks = useTracker(() =>
    Tasks.find({}, { sort: { createdAt: -1 } }).fetch()
  );
  const [taskVal, setTaskVal] = useState("");

  const handleSubmit = event => {
    event.preventDefault();

    Tasks.insert({
      text: taskVal,
      createdAt: new Date()
    });

    setTaskVal("");
  };

  const renderTasks = () => {
    return tasks.map(task => <Task key={task._id} task={task} />);
  };

  return (
    <div>
      <div className="container">
        <header>
          <h1>Todo List</h1>
          <ul>{renderTasks()}</ul>
        </header>
        <form className="new-task" onSubmit={handleSubmit}>
          <input
            type="text"
            value={taskVal}
            placeholder="Type to add new tasks"
            onChange={event => setTaskVal(event.target.value)}
          />
        </form>
      </div>
    </div>
  );
};
